void setup(){
size(1920,1080,P3D);
camera(0,0,Window.eyeZ,
       0,0,0,
       0,-1,0);
 filter(BLUR,6);
}
Walker walker= new Walker();
  Vector2 mousePos(){
    float x =mouseX -Window.windowWidth;
    float y = -(mouseY-Window.windowHeight);
    return new Vector2(x,y);
  }
void draw(){
  background(0);
 Vector2 mouse = mousePos();

 mouse.normalize();
 mouse.multi(400);
 strokeWeight(10);
 stroke(200,0,0);
 line(0,0,mouse.x,mouse.y);
 strokeWeight(3);
 stroke(255);
 line(0,0,mouse.x,mouse.y);
 scale(-1,-1);
 strokeWeight(10);
 stroke(200,0,0);
 line(0,0,mouse.x,mouse.y);
 strokeWeight(2);
 stroke(255);
 line(0,0,mouse.x,mouse.y);
  mouse.normalize();
 mouse.multi(50);
 scale(1,1);
 strokeWeight(15);
 stroke(230);
 line(0,0,mouse.x,mouse.y);
  scale(-1,-1);
 strokeWeight(15);
 stroke(230);
 line(0,0,mouse.x,mouse.y);
}
