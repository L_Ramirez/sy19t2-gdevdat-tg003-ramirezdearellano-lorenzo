public class Vector2
{
  public float x;
  public float y;
  
  Vector2(){
    x=0;
    y=0;
  }
  
  Vector2(float x, float y){
    this.x=x;
    this.y=y;
  }
  
  public void add(Vector2 u){
    this.x+=u.x;
    this.y+=u.y;
  }
  
  public void subtract(Vector2 u){
    this.x-=u.x;
    this.y-=u.y;
  }
  
    public void multi(float scalar){
    this.x*=scalar;
    this.y*=scalar;
  }
    public void divide(float scalar){
    this.x/=scalar;
    this.y/=scalar;
  }
  public float mag(){
    return sqrt((x*x)+(y*y));
  }
  
  public void normalize(){
    float length=this.mag();
    if (length>0){
      this.divide(length);
    }
  }
  
  Vector2 mousePos(){
    float x =mouseX -Window.windowWidth;
    float y = -(mouseY-Window.windowHeight);
    return new Vector2(x,y);
  }
}
