float x,y;
void setup(){
size(1920,1080,P3D);
camera(0,0,-(height/2)/tan(PI*30/180),
       0,0,0,
       0,-1,0);
       background(130);

}
void draw(){
   drawSplatter();
   if (frameCount % 100 == 0) {
     background(130);
}
}


void drawSplatter(){
  fill(random(255),random(255),random(255));
  circle(randomGaussian()*180,randomGaussian()*180,randomGaussian()*50);
  
}
