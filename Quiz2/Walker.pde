class Walker
{
  float xPosition;
  float yPosition;
  
  Walker(){
  xPosition = 0;
  yPosition = 0;
}
  Walker(float x, float y){
  this.xPosition = x;
  this.yPosition = y;
  
  }
  float dt3= 200;
  void render(){
    fill(noise(dtx)*255,noise(dty)*255,noise(dt3)*255);
    circle(xPosition, yPosition,noise(dtx)*100);
  }
  float dtx=0;
  float dty=500;
  void walk(float wh,float wr){
  float nx = noise(dtx);
  float ny = noise (dty);
  xPosition = map(nx,0,1,-wr,wr);
  dtx+=0.01f;
  yPosition = map(ny,0,1,-wh,wh);
   dty+=0.01f;
   dt3+=0.01f;
  }
}
