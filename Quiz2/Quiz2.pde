
public static class Window
{
  public static int widthPx=1920;
  public static int heightPx=1080;
  public static int windowWidth = widthPx/2;
  public static int windowHeight =heightPx/2;
  public static int top=windowHeight;
  public static int bottom = -windowHeight;
  public static int left = -windowWidth;
  public static int right = windowWidth;
};
void setup(){
size(1920,1080,P3D);
camera(0,0,-(height/2)/tan(PI*30/180),
       0,0,0,
       0,-1,0);
}
int x;
Walker walker= new Walker();

void draw(){
  noStroke();
  walker.render();
  walker.walk(Window.top,Window.right);
}
